const User = require("../models/Users");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.register = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
    mobileNo: reqBody.mobileNo,
  });
  return newUser.save().then((result, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

module.exports.checkEmail = (email) => {
  return User.findOne({ email: email }).then((result, error) => {
    if (result != null) {
      return `Email already exists`;
    } else {
      if (result) {
        return false;
      } else {
        return true;
      }
    }
  });
};

module.exports.getAllUsers = () => {
  return User.find().then((result) => {
    if (!result) {
      return false;
    } else {
      return result;
    }
  });
};

module.exports.login = (reqBody) => {
  const { email, password } = reqBody;
  return User.findOne({ email: email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(password, result.password);
      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};

module.exports.getProfile = (id) => {
  return User.findOne({ _id: id }).then((result) => {
    if (result) {
      result.password = "";
      return result;
    } else {
      return false;
    }
  });
};
