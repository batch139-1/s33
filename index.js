const express = require("express");
const port = process.env.port || 4000;
const app = express();
const mongoose = require("mongoose");
const courseRoutes = require("./routes/courseRoutes");
const userRoutes = require("./routes/userRoutes");

mongoose.connect(
    "mongodb+srv://admin:admin@batch139.kdoph.mongodb.net/course-booking",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => console.log("Connected to database"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/api/courses", courseRoutes);
app.use("/api/users", userRoutes);

app.listen(port, () => console.log(`connected to localhost:${port}`));
