const express = require("express");
const userController = require("../controllers/userControllers");
const router = express.Router();
const auth = require("../auth");

router.post("/register", (req, res) => {
  userController.register(req.body).then((result) => {
    res.send(result);
  });
});

router.get("/email-exists", (req, res) => {
  userController.checkEmail(req.body.email).then((result) => {
    res.send(result);
  });
});

router.get("/", (req, res) => {
  userController.getAllUsers().then((result) => {
    res.send(result);
  });
});

router.post("/login", (req, res) => {
  userController.login(req.body).then((result) => {
    res.send(result);
  });
});

// retrieve user information
router.get("/details", auth.verify, (req, res) => {
  let userData = auth.decode(req.headers.authorization);
  console.log(userData);
  userController.getProfile(req.body.id).then((result) => {
    res.send(result);
  });
});

module.exports = router;
